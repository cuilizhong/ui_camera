//
//  BeautyFunctionView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "BasicView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BeautyFunctionView : BasicView

@property(nonatomic,strong)void(^dismiss)(void);

@property(nonatomic,strong)void(^confirm)(void);


@end

NS_ASSUME_NONNULL_END
