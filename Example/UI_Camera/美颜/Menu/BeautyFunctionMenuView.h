//
//  BeautyFunctionMenuView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/18.
//

#import "BasicView.h"
#import "BeautyFunctionMenuViewVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface BeautyFunctionMenuView : BasicView

@property(nonatomic,strong)BeautyFunctionMenuViewVM *beautyFunctionMenuViewVM;

@end

NS_ASSUME_NONNULL_END
