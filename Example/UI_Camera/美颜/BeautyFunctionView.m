//
//  BeautyFunctionView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "BeautyFunctionView.h"
#import "BeautyFunctionContentView.h"
#import "FilterMenuView.h"

@interface BeautyFunctionView()

@property(nonatomic,strong)FilterMenuView *filterMenuView;

@property(nonatomic,strong)BeautyFunctionContentView *beautyFunctionContentView;

@end

@implementation BeautyFunctionView

- (instancetype)init{
    self = [super init];
    if (self) {
                
        UIView *topView = [[UIView alloc]init];
        [self addSubview:topView];
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.mas_equalTo(50);
        }];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        cancelButton.titleLabel.font = Font(14);
        [topView addSubview:cancelButton];
        [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(30, 30));
            make.left.equalTo(topView).offset(10);
            make.centerY.equalTo(topView);
        }];
        
        UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirmButton addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
        [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        confirmButton.titleLabel.font = Font(14);
        [topView addSubview:confirmButton];
        [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(30, 30));
            make.right.equalTo(topView).offset(-10);
            make.centerY.equalTo(topView);
        }];
        
        
        self.filterMenuView = [[FilterMenuView alloc]init];
        self.filterMenuView.backgroundColor = [UIColor clearColor];
        [topView addSubview:self.filterMenuView];
        [self.filterMenuView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(cancelButton.mas_right).offset(20);
            make.right.equalTo(confirmButton.mas_left).offset(-20);
            make.centerY.equalTo(topView);
            make.height.mas_equalTo(50);
        }];
        UIView *placeholdView = [[UIView alloc]init];
        [self addSubview:placeholdView];
        [placeholdView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView.mas_bottom);
            make.bottom.equalTo(self);
        }];
        
        self.beautyFunctionContentView = [[BeautyFunctionContentView alloc]init];
        [self addSubview:self.beautyFunctionContentView];
        [self.beautyFunctionContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.mas_equalTo(50);
            make.centerY.equalTo(placeholdView);
        }];
        
    }
    return self;
}

- (void)cancel:(UIButton *)sender{
    if (self.dismiss) {
        self.dismiss();
    }
}

- (void)confirm:(UIButton *)sender{
    if (self.confirm) {
        self.confirm();
    }
}


- (void)dealloc{
    
}

@end
