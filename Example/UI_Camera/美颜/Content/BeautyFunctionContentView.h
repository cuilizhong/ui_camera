//
//  BeautyFunctionContentView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/18.
//

#import "BasicView.h"
#import "BeautyFunctionContentViewVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface BeautyFunctionContentView : BasicView

@property(nonatomic,strong)BeautyFunctionContentViewVM *beautyFunctionContentViewVM;

@end

NS_ASSUME_NONNULL_END
