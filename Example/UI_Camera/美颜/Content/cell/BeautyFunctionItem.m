//
//  BeautyFunctionItem.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/18.
//

#import "BeautyFunctionItem.h"

@interface BeautyFunctionItem()

@property(nonatomic,strong)UIImageView *contentImageView;
@property(nonatomic,strong)UILabel *contentLabel;

@end

@implementation BeautyFunctionItem

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentImageView = [[UIImageView alloc]init];
        self.contentImageView.image = [UIImage imageNamed:@"拍照"];
        [self.contentView addSubview:self.contentImageView];
        [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.centerX.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(35, 35));
        }];
        
        self.contentLabel = [[UILabel alloc]init];
        self.contentLabel.text = @"测试";
        self.contentLabel.textColor = [UIColor whiteColor];
        self.contentLabel.font = Font(12);
        self.contentLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.contentView);
        }];
        
    }
    return self;
}

@end
