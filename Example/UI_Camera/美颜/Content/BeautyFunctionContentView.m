//
//  BeautyFunctionContentView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/18.
//

#import "BeautyFunctionContentView.h"
#import "BeautyFunctionItem.h"

@interface BeautyFunctionContentView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;

@end

@implementation BeautyFunctionContentView

- (instancetype)init{
    
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        self.beautyFunctionContentViewVM = [[BeautyFunctionContentViewVM alloc]init];
        
        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        layout.minimumLineSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0,20, 0, 0);
        layout.itemSize = CGSizeMake(26,45);
        layout.minimumLineSpacing = 30;
        //2.初始化collectionView
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        [self.collectionView registerClass:[BeautyFunctionItem class] forCellWithReuseIdentifier:@"BeautyFunctionItem"];
        
    }
    return self;
}

#pragma mark-UICollectionViewDelegate,UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BeautyFunctionItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BeautyFunctionItem" forIndexPath:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(50, collectionView.frame.size.height);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


- (void)dealloc{
    
}


@end
