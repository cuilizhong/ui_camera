//
//  AddPhtotoCell.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "AddPhtotoCell.h"

@implementation AddPhtotoCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = ColorValue(0X333333);
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:@"addphoto"];
        [self.contentView addSubview:imageView];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(20, 20));
            make.center.equalTo(self.contentView);
        }];
    }
    return self;
}

@end
