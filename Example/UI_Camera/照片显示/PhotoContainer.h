//
//  PhotoContainer.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import <UIKit/UIKit.h>
#import "PhotoContainerVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoContainer : UIView

@property(nonatomic,strong)PhotoContainerVM *photoContainerVM;

@property(nonatomic,strong)void(^selectedImage)(UIImage *image);

@property(nonatomic,strong)void(^cancelSelected)(void);

@property(nonatomic,strong)void(^deletePhoto)(UIImage *photo);

@property(nonatomic,strong)void(^takePhoto)(void);

@end

NS_ASSUME_NONNULL_END
