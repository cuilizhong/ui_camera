//
//  PhotoContainerCell.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import <UIKit/UIKit.h>
#import "PhotoContainerCellVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoContainerCell : UICollectionViewCell

@property(nonatomic,strong)PhotoContainerCellVM *photoContainerCellVM;

@property(nonatomic,strong)void(^deletePhoto)(void);

@end

NS_ASSUME_NONNULL_END
