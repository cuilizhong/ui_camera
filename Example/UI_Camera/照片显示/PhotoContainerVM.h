//
//  PhotoContainerVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotoContainerVM : NSObject

@property(nonatomic,strong)NSMutableArray *photos;

@end

NS_ASSUME_NONNULL_END
