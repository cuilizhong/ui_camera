//
//  PhotoContainerVM.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "PhotoContainerVM.h"

@implementation PhotoContainerVM

- (instancetype)init{
    self = [super init];
    if (self) {
        self.photos = [[NSMutableArray alloc]init];
    }
    return self;
}

@end
