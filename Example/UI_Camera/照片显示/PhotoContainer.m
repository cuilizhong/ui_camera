//
//  PhotoContainer.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "PhotoContainer.h"
#import "PhotoContainerCell.h"
#import "AddPhtotoCell.h"

@interface PhotoContainer()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;

@property(nonatomic,assign)int selectedIndex;


@end


@implementation PhotoContainer

- (instancetype)init{
    
    self = [super init];
    if (self) {
        
        self.selectedIndex = -1;
        
        self.photoContainerVM = [[PhotoContainerVM alloc]init];
        [self.photoContainerVM addObserver:self forKeyPath:@"photos" options:NSKeyValueObservingOptionNew context:nil];
        
        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        layout.minimumLineSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0,20, 0, 0);
        layout.itemSize = CGSizeMake(50,50);
        layout.minimumLineSpacing = 14;
        layout.minimumInteritemSpacing = 14;
        
        //2.初始化collectionView
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        [self.collectionView registerClass:[PhotoContainerCell class] forCellWithReuseIdentifier:@"PhotoContainerCell"];
        
        [self.collectionView registerClass:[AddPhtotoCell class] forCellWithReuseIdentifier:@"AddPhtotoCell"];
    }
    return self;
}

#pragma mark-UICollectionViewDelegate,UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.photoContainerVM.photos.count<9?self.photoContainerVM.photos.count+1:self.photoContainerVM.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.photoContainerVM.photos.count<9 && (indexPath.row == self.photoContainerVM.photos.count)) {
        //拍照
        AddPhtotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AddPhtotoCell" forIndexPath:indexPath];
        
        return cell;
        
    }else{
        
        PhotoContainerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoContainerCell" forIndexPath:indexPath];
        UIImage *image = self.photoContainerVM.photos[indexPath.row];
        cell.photoContainerCellVM.contentImage = image;
        cell.photoContainerCellVM.isSelected = self.selectedIndex==indexPath.row?YES:NO;
        @WeakObj(self);
        cell.deletePhoto = ^{
            if (selfWeak.deletePhoto) {
                selfWeak.deletePhoto(image);
            }
        };
        
        return cell;
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.photoContainerVM.photos.count<9 && (indexPath.row == self.photoContainerVM.photos.count)) {
        
        if (self.selectedIndex != -1) {
            self.selectedIndex = -1;
            [self.collectionView reloadData];
            if (self.cancelSelected) {
                self.cancelSelected();
            }
        }else{
            if (self.takePhoto) {
                self.takePhoto();
            }
        }
    }else{
        if (self.selectedIndex == indexPath.row) {
            self.selectedIndex = -1;
            if (self.cancelSelected) {
                self.cancelSelected();
            }
        }else{
            self.selectedIndex = (int)indexPath.row;
            UIImage *image = self.photoContainerVM.photos[indexPath.row];
            if (self.selectedImage) {
                self.selectedImage(image);
            }
        }
        [self.collectionView reloadData];
    }
    
}

#pragma mark-监听
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"photos"]) {
        [self.collectionView reloadData];
    }
}


- (void)dealloc{
    [self.photoContainerVM removeObserver:self forKeyPath:@"photos"];
}


@end
