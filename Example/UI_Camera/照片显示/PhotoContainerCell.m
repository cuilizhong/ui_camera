//
//  PhotoContainerCell.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "PhotoContainerCell.h"

@interface PhotoContainerCell()

@property(nonatomic,strong)UIImageView *imageView;

@end

@implementation PhotoContainerCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.photoContainerCellVM = [[PhotoContainerCellVM alloc]init];
        
        [self.photoContainerCellVM addObserver:self forKeyPath:@"contentImage" options:NSKeyValueObservingOptionNew context:nil];
        
        [self.photoContainerCellVM addObserver:self forKeyPath:@"isSelected" options:NSKeyValueObservingOptionNew context:nil];
        
        self.imageView = [[UIImageView alloc]init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        [self.contentView addSubview:self.imageView];
        [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.contentView).offset(2);
            make.right.bottom.equalTo(self.contentView).offset(-2);
        }];
        
        UIButton *delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [delBtn setImage:[UIImage imageNamed:@"photo_delete"] forState:UIControlStateNormal];
        [delBtn addTarget:self action:@selector(deletePhoto:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:delBtn];
        [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(20, 20));
            make.top.right.equalTo(self.contentView);
        }];
        
    }
    return self;
}

- (void)deletePhoto:(UIButton *)sender{
    if (self.deletePhoto) {
        self.deletePhoto();
    }
}

#pragma mark-监听
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"contentImage"]) {
        
        UIImage *contentImage = value;
        if ([contentImage isKindOfClass:[UIImage class]]) {
            self.imageView.image = contentImage;
        }
        
    }else if ([keyPath isEqualToString:@"isSelected"]) {
        
        BOOL isSelected = [self filterData:value].boolValue;
        if (isSelected) {
            
            self.imageView.layer.borderColor = [UIColor blueColor].CGColor;
            self.imageView.layer.borderWidth = 2;
            
        }else{
            
            self.imageView.layer.borderColor = [UIColor clearColor].CGColor;
            self.imageView.layer.borderWidth = 0;
        }
    }
}

- (void)dealloc{
    [self.photoContainerCellVM removeObserver:self forKeyPath:@"contentImage"];
    [self.photoContainerCellVM removeObserver:self forKeyPath:@"isSelected"];
}

@end
