//
//  PhotoContainerCellVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/20.
//

#import "BasicViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoContainerCellVM : BasicViewModel

@property(nonatomic,strong)UIImage *contentImage;

@property(nonatomic,assign)BOOL isSelected;

@end

NS_ASSUME_NONNULL_END
