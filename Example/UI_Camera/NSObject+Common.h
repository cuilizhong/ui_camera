//
//  NSObject+Common.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Common)

-(NSString *)getHHMMSSFromSS:(NSInteger)seconds;

- (NSString *)getDateString;
- (NSString *)getCurrentDateStr;

-(NSString *)filterData:(id)data;

- (BOOL)filterNilData:(id)data;

- (BOOL)isArrayType:(id)data;

- (BOOL)isDictionaryType:(id)data;


@end

NS_ASSUME_NONNULL_END
