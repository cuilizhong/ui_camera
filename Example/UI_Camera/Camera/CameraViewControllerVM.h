//
//  CameraViewControllerVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicViewModel.h"
#import "CameraFunctionModel.h"

typedef NS_ENUM(NSInteger,CameraOperationType)
{
    CameraType_TakePhoto,//拍照
    CameraType_TakeVideo,//拍视频
};

typedef NS_ENUM(NSInteger,BottomShowToolType)
{
    BottomShowToolType_Original,//拍照、拍视频
    BottomShowToolType_Photos,//显示照片
    BottomShowToolType_Edit,//编辑
    BottomShowToolType_Beauty,//美颜
    BottomShowToolType_Filter,//滤镜
    BottomShowToolType_VideoEdit,//视频编辑
};

typedef NS_ENUM(NSInteger,RecordStatus)
{
    RecordStatus_RecordPrepare,//准备录制
    RecordStatus_Recording,//正在录制
    RecordStatus_RecordEnd,//录制结束
};

NS_ASSUME_NONNULL_BEGIN

@interface CameraViewControllerVM : BasicViewModel

@property(nonatomic,assign)CameraOperationType cameraOperationType;

@property(nonatomic,strong)CameraFunctionModel * currentCameraFunctionModel;

@property(nonatomic,assign)BottomShowToolType bottomShowToolType;

@property(nonatomic,assign)RecordStatus recordStatus;

@property(nonatomic,strong)NSMutableArray <UIImage *> *photosArray;
@property(nonatomic,strong)NSString *localVideoPath;

- (void)uploadPhotoWithSuccess:(void(^)(NSMutableArray *urls))success fail:(void(^)(NSString *errorMessage))fail;

- (void)uploadVideoWithFileName:(NSString *)fileName success:(void(^)(NSString *videoURL))success fail:(void(^)(NSString *errorMessage))fail;


+(CameraFunctionModel *)getEdit_Photo;

+(CameraFunctionModel *)getBeauty_Photo;

+(CameraFunctionModel *)getFilter_Photo;

+(CameraFunctionModel *)getCut_Photo;

+(CameraFunctionModel *)getRotate_Photo;

+(CameraFunctionModel *)getBrush_Photo;

+(CameraFunctionModel *)getMosaic_Photo;

+(CameraFunctionModel *)getBeauty_Video;

+(CameraFunctionModel *)getFilter_Video;

+(CameraFunctionModel *)getAddText_Video;

+(CameraFunctionModel *)getCut_Video;

+(CameraFunctionModel *)getAR_Video;

@end

NS_ASSUME_NONNULL_END
