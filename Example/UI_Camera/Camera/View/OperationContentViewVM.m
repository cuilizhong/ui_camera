//
//  OperationContentViewVM.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "OperationContentViewVM.h"

@implementation OperationContentViewVM

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.photos = [[NSMutableArray alloc]init];
    }
    return self;
}

@end
