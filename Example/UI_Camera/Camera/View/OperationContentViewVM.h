//
//  OperationContentViewVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicViewModel.h"
#import "CameraFunctionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OperationContentViewVM : BasicViewModel

@property(nonatomic,strong)NSMutableArray *photos;

@end

NS_ASSUME_NONNULL_END
