//
//  OperationContentView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicView.h"
#import "OperationContentViewVM.h"

NS_ASSUME_NONNULL_BEGIN


@interface OperationContentView : BasicView

@property(nonatomic,strong)OperationContentViewVM *operationContentViewVM;

@property(nonatomic,strong)void(^takePhoto)(void);

@property(nonatomic,strong)void(^selectedImage)(UIImage *image);
@property(nonatomic,strong)void(^deletePhoto)(UIImage *photo);

@property(nonatomic,strong)void(^cancelSelected)(void);


@property(nonatomic,strong)void(^submit)(void);

@property(nonatomic,strong)void(^function)(CameraFunctionModel *model);

@end

NS_ASSUME_NONNULL_END
