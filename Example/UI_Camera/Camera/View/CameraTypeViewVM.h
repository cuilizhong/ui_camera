//
//  CameraTypeViewVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CameraTypeViewVM : BasicViewModel

@property(nonatomic,assign)BOOL isSelected;

@property(nonatomic,strong)NSString *contentStr;

@end

NS_ASSUME_NONNULL_END
