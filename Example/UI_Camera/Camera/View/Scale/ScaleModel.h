//
//  ScaleModel.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/20.
//

#import "BasicModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScaleModel : BasicModel

@property(nonatomic,assign)NSInteger widthScale;

@property(nonatomic,assign)NSInteger heightScale;

@end

NS_ASSUME_NONNULL_END
