//
//  ScaleViewVM.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/20.
//

#import "ScaleViewVM.h"

@implementation ScaleViewVM

- (instancetype)init{
    self = [super init];
    if (self) {
        self.dataArray = [[NSMutableArray alloc]init];
        for (int i = 0; i<3; i++) {
            ScaleModel *model = [[ScaleModel alloc]init];
            if (i == 0) {
                model.widthScale = 1;
                model.heightScale = 1;
            }else if (i == 1){
                model.widthScale = 3;
                model.heightScale = 4;
            }else if (i == 2){
                model.widthScale = 9;
                model.heightScale = 16;
            }
            [self.dataArray addObject:model];
        }
        
        self.contentStr = [NSString stringWithFormat:@"%ld:%ld",self.dataArray.firstObject.heightScale,self.dataArray.firstObject.widthScale];
    }
    return self;
}

@end
