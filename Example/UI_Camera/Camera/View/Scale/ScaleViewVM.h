//
//  ScaleViewVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/20.
//

#import "BasicViewModel.h"
#import "ScaleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScaleViewVM : BasicViewModel

@property(nonatomic,strong)NSString *contentStr;

@property(nonatomic,strong)NSMutableArray <ScaleModel *>*dataArray;

@end

NS_ASSUME_NONNULL_END
