//
//  ScaleView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicView.h"
#import "ScaleViewVM.h"


NS_ASSUME_NONNULL_BEGIN

@interface ScaleView : BasicView

@property(nonatomic,strong)ScaleViewVM *scaleViewVM;

@property(nonatomic,strong)void(^selectedScale)(CGFloat scale);

@end

NS_ASSUME_NONNULL_END
