//
//  ScaleCell.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/20.
//

#import "ScaleCell.h"

@interface ScaleCell()


@end

@implementation ScaleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        
        UIView *contentView = [[UIView alloc]init];
        contentView.layer.cornerRadius = 7;
        contentView.layer.borderColor = [UIColor whiteColor].CGColor;
        contentView.layer.borderWidth = 1;
        [self.contentView addSubview:contentView];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self.contentView);
            make.top.equalTo(self.contentView).offset(8);
        }];
        
        self.contentLabel = [[UILabel alloc]init];
        self.contentLabel.font = Font(15);
        self.contentLabel.textColor = [UIColor whiteColor];
        self.contentLabel.textAlignment = NSTextAlignmentCenter;
        [contentView addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(contentView);
        }];
    }
    return self;
}

@end
