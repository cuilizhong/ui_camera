//
//  ScaleView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "ScaleView.h"
#import "ScaleCell.h"

@interface ScaleView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UILabel *contentLabel;

@end

@implementation ScaleView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.scaleViewVM = [[ScaleViewVM alloc]init];
        
        UIView *contentView = [[UIView alloc]init];
        contentView.layer.cornerRadius = 7;
        contentView.layer.borderColor = [UIColor whiteColor].CGColor;
        contentView.layer.borderWidth = 1;
        [self addSubview:contentView];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.mas_equalTo(22);
        }];
        
        self.contentLabel = [[UILabel alloc]init];
        self.contentLabel.font = Font(15);
        self.contentLabel.textColor = [UIColor whiteColor];
        self.contentLabel.textAlignment = NSTextAlignmentCenter;
        self.contentLabel.text = self.scaleViewVM.contentStr;
        [contentView addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(contentView);
        }];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundColor:[UIColor clearColor]];
        [button addTarget:self action:@selector(selectedScale:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(contentView);
        }];
        
        self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.rowHeight =UITableViewAutomaticDimension;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
        [self addSubview:self.tableView];
        [self.tableView registerClass:[ScaleCell class] forCellReuseIdentifier:@"ScaleCell"];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.top.equalTo(contentView.mas_bottom).offset(5);
        }];
        self.tableView.hidden = YES;
    }
    return self;
}

- (void)selectedScale:(UIButton *)sender{
    self.tableView.hidden = !self.tableView.hidden;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.scaleViewVM.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ScaleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScaleCell" forIndexPath:indexPath];
    
    ScaleModel *model = self.scaleViewVM.dataArray[indexPath.row];
    NSString *contentStr = [NSString stringWithFormat:@"%ld:%ld",(long)model.heightScale,model.widthScale];
    cell.contentLabel.text = contentStr;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ScaleModel *model = self.scaleViewVM.dataArray[indexPath.row];
    NSString *contentStr = [NSString stringWithFormat:@"%ld:%ld",(long)model.heightScale,model.widthScale];
    self.contentLabel.text = contentStr;
    if (self.selectedScale) {
        self.selectedScale(model.heightScale*1.0/model.widthScale);
    }
    self.tableView.hidden = YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (void)dealloc{
}
@end
