//
//  ScaleCell.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ScaleCell : UITableViewCell

@property(nonatomic,strong)UILabel *contentLabel;


@end

NS_ASSUME_NONNULL_END
