//
//  CameraTypeView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicView.h"
#import "CameraTypeViewVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface CameraTypeView : BasicView

@property(nonatomic,strong)CameraTypeViewVM *cameraTypeViewVM;

@end

NS_ASSUME_NONNULL_END
