//
//  OperationContentView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "OperationContentView.h"
#import "PhotoContainer.h"
#import "FunctionMenuView.h"

@interface OperationContentView()

@property(nonatomic,strong)PhotoContainer *photoContainer;

@property(nonatomic,strong)FunctionMenuView *functionMenuView;

@property(nonatomic,strong)UIView *maskView;

@end


@implementation OperationContentView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.operationContentViewVM = [[OperationContentViewVM alloc]init];
        [self.operationContentViewVM addObserver:self forKeyPath:@"photos" options:NSKeyValueObservingOptionNew context:nil];
        
        self.photoContainer = [[PhotoContainer alloc]init];
        @WeakObj(self);
        self.photoContainer.takePhoto = ^{
            if (selfWeak.takePhoto) {
                selfWeak.takePhoto();
            }
        };
        
        self.photoContainer.deletePhoto = ^(UIImage * _Nonnull photo) {
            if (selfWeak.deletePhoto) {
                selfWeak.deletePhoto(photo);
            }
        };
        
        self.photoContainer.cancelSelected = ^{
            selfWeak.maskView.hidden = NO;
            if (selfWeak.cancelSelected) {
                selfWeak.cancelSelected();
            }
        };
        
        self.photoContainer.selectedImage = ^(UIImage * _Nonnull image) {
            selfWeak.maskView.hidden = YES;
            if (selfWeak.selectedImage) {
                selfWeak.selectedImage(image);
            }
        };
        
        self.photoContainer.backgroundColor = [UIColor clearColor];
        [self addSubview:self.photoContainer];
        [self.photoContainer mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(30);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.mas_equalTo(50);
            
        }];
        
        UIButton *nextStepButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextStepButton setBackgroundColor:[UIColor whiteColor]];
        [nextStepButton setTitleColor:ColorValue(0x333333) forState:UIControlStateNormal];
        [nextStepButton setTitle:@"下一步" forState:UIControlStateNormal];
        nextStepButton.layer.cornerRadius = 14;
        nextStepButton.titleLabel.font = Font(13);
        [nextStepButton addTarget:self action:@selector(nextStep:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:nextStepButton];
        [nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-22);
            make.size.mas_equalTo(CGSizeMake(70, 28));
            make.bottom.equalTo(self).offset(-30);
        }];
        
        self.functionMenuView = [[FunctionMenuView alloc]init];
        self.functionMenuView.function = ^(CameraFunctionModel * _Nonnull model) {
            if (selfWeak.function) {
                selfWeak.function(model);
            }
        };
        [self addSubview:self.functionMenuView];
        [self.functionMenuView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(60);
            make.left.equalTo(self);
            make.right.equalTo(nextStepButton.mas_left).offset(-20);
            make.centerY.equalTo(nextStepButton);
        }];
        
        self.maskView = [[UIView alloc]init];
        self.maskView.backgroundColor = ColorValueWithAlpha(0x000000, 0.6);
        [self addSubview:self.maskView];
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.functionMenuView);
        }];
        
    }
    return self;
}

- (void)nextStep:(UIButton *)sender{
    if (self.submit) {
        self.submit();
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"photos"]) {
        self.photoContainer.photoContainerVM.photos = value;
    }
}

- (void)dealloc{
    [self.operationContentViewVM removeObserver:self forKeyPath:@"photos"];
}

@end
