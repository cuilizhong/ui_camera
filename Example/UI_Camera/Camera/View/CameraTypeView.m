//
//  CameraTypeView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "CameraTypeView.h"

@interface CameraTypeView()

@property(nonatomic,strong)UILabel *contentLabel;

@property(nonatomic,strong)UIView *whitePoint;

@end

@implementation CameraTypeView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.cameraTypeViewVM = [[CameraTypeViewVM alloc]init];
        
        [self.cameraTypeViewVM addObserver:self forKeyPath:@"isSelected" options:NSKeyValueObservingOptionNew context:nil];
        
        [self.cameraTypeViewVM addObserver:self forKeyPath:@"contentStr" options:NSKeyValueObservingOptionNew context:nil];

        
        self.contentLabel = [[UILabel alloc]init];
        self.contentLabel.textColor = [UIColor whiteColor];
        self.contentLabel.font = Font(13);
        self.contentLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
        }];
        
        self.whitePoint = [[UIView alloc]init];
        self.whitePoint.layer.cornerRadius = 2;
        self.whitePoint.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.whitePoint];
        [self.whitePoint mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.contentLabel.mas_bottom).offset(5);
            make.bottom.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(4, 4));
        }];
        
        
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"isSelected"]) {
        
        BOOL isSelected = [self filterData:value].boolValue;
        self.whitePoint.hidden = !isSelected;
        if (isSelected) {
            self.contentLabel.textColor = [UIColor whiteColor];
            self.contentLabel.font = Font(15);
        }else{
            self.contentLabel.textColor = ColorValue(0XCCCCCC);
            self.contentLabel.font = Font(13);
        }
        
    }else if ([keyPath isEqualToString:@"contentStr"]){
        
        NSString *contentStr = [self filterData:value];
        self.contentLabel.text = contentStr;
    }
}

- (void)dealloc{
    [self.cameraTypeViewVM removeObserver:self forKeyPath:@"isSelected"];
    [self.cameraTypeViewVM removeObserver:self forKeyPath:@"contentStr"];
}

@end
