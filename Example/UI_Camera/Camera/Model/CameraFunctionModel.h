//
//  CameraFunctionModel.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "BasicModel.h"
typedef NS_ENUM(NSInteger,FunctionMenuType){
    FunctionMenuType_Edit_Photo,//编辑
    FunctionMenuType_Beauty_Photo,//美颜
    FunctionMenuType_Filter_Photo,//滤镜
    FunctionMenuType_Cut_Photo,//裁剪照片
    FunctionMenuType_Rotate_Photo,//旋转照片
    FunctionMenuType_Brush_Photo,//画笔照片
    FunctionMenuType_Mosaic_Photo,//画笔照片

    FunctionMenuType_Beauty_Video,//美颜
    FunctionMenuType_Filter_Video,//滤镜
    FunctionMenuType_AddText_Video,//文字
    FunctionMenuType_Cut_Video,//剪辑
    FunctionMenuType_AR_Video,//AR
};

NS_ASSUME_NONNULL_BEGIN

@interface CameraFunctionModel : BasicModel

@property(nonatomic,assign)FunctionMenuType functionMenuType;
@property(nonatomic,strong)UIImage *functionMenuImage;
@property(nonatomic,strong)NSString *functionMenuName;

@end

NS_ASSUME_NONNULL_END
