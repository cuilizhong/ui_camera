//
//  CameraViewController.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicViewController.h"
#import "CameraViewControllerVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface CameraViewController : BasicViewController

@property(nonatomic,strong)CameraViewControllerVM *cameraViewControllerVM;


/// 录制的最大时长（默认30秒）
@property(nonatomic,assign)NSInteger maxRecordDuration;

@property(nonatomic,strong)void(^getVideoURL)(NSString *videoURL);

@property(nonatomic,strong)void(^getPhotos)(NSMutableArray <NSString *>*photos);
@end

NS_ASSUME_NONNULL_END
