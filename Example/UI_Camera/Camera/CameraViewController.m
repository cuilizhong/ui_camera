//
//  CameraViewController.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "CameraViewController.h"
#import "ScaleView.h"
#import "OperationContentView.h"
#import "CameraTypeView.h"

#import "FilterFunctionView.h"
#import "BeautyFunctionView.h"
#import "EditFunctionView.h"
#import "VideoEditFunctionView.h"
#import <GTVideoLib/GTVideoLib.h>
#import "MLMCircleView.h"



static CGFloat bottomHeight = 180;

@interface CameraViewController ()<GTVRecControllerDelegate,GTVReplayControllerDelegate>

@property(nonatomic,strong)UIView *preview;
@property(nonatomic,strong)UIView *cameraFuncView;
@property(nonatomic,strong)ScaleView *scaleView;
@property(nonatomic,strong)UIButton *flashLampButton;
@property(nonatomic,strong)UILabel *recordTimeLabel;

@property(nonatomic,strong)UIView *bottomContentView;
@property(nonatomic,strong)CameraTypeView *photoCamera;
@property(nonatomic,strong)CameraTypeView *videoCamera;


@property(nonatomic,strong)UIButton *actionButton;
@property(nonatomic,strong)MLMCircleView *circle;



//显示图片和功能按钮
@property(nonatomic,strong)OperationContentView *operationContentView;

//视频编辑功能
@property(nonatomic,strong)VideoEditFunctionView *videoEditFunctionView;

//滤镜功能
@property(nonatomic,strong)FilterFunctionView *filterFunctionView;
//美颜功能
@property(nonatomic,strong)BeautyFunctionView *beautyFunctionView;
//编辑功能
@property(nonatomic,strong)EditFunctionView *editFunctionView;




@property(nonatomic,strong)GTVRecController *gTVRecController;
@property(nonatomic,strong)GTVReplayController *gTVReplayController;
@property(nonatomic,strong)UIImageView *contentPhoto;
@property(nonatomic,strong)UIView *contentVideo;

@end

@implementation CameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    self.cameraViewControllerVM = [[CameraViewControllerVM alloc]init];
    
    [self.cameraViewControllerVM addObserver:self forKeyPath:@"photosArray" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.cameraViewControllerVM addObserver:self forKeyPath:@"cameraOperationType" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.cameraViewControllerVM addObserver:self forKeyPath:@"bottomShowToolType" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.cameraViewControllerVM addObserver:self forKeyPath:@"recordStatus" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.cameraViewControllerVM addObserver:self forKeyPath:@"currentCameraFunctionModel" options:NSKeyValueObservingOptionNew context:nil];
    if (!(self.maxRecordDuration>0)) {
        self.maxRecordDuration = 30;
    }

    self.preview = [[UIView alloc]init];
    self.preview.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.preview];
    [self.preview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        } else {
            make.top.equalTo(self.view);
        }
    }];
    
    
    self.cameraFuncView = [[UIView alloc]init];
    self.cameraFuncView.backgroundColor = [UIColor clearColor];
    [self.preview addSubview:self.cameraFuncView];
    [self.cameraFuncView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.preview);
        make.height.mas_equalTo(126);
    }];
    
    UIView *cameraFuncSubView = [[UIView alloc]init];
    cameraFuncSubView.backgroundColor = ColorValueWithAlpha(0x000000, 0.3);
    [self.cameraFuncView addSubview:cameraFuncSubView];
    [cameraFuncSubView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.cameraFuncView);
        make.height.mas_equalTo(40);
    }];
    
    self.recordTimeLabel = [[UILabel alloc]init];
    self.recordTimeLabel.font = Font(13);
    self.recordTimeLabel.textColor = [UIColor whiteColor];
    self.recordTimeLabel.textAlignment = NSTextAlignmentCenter;
    [cameraFuncSubView addSubview:self.recordTimeLabel];
    [self.recordTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(cameraFuncSubView);
    }];
    
    //关闭
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closePage:) forControlEvents:UIControlEventTouchUpInside];
    [cameraFuncSubView addSubview:closeButton];
    [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.equalTo(cameraFuncSubView);
        make.width.mas_equalTo(40);
    }];
    
    //比例控制
    self.scaleView = [[ScaleView alloc]init];
    self.scaleView.selectedScale = ^(CGFloat scale) {
        DebugLog(@"设置比例 = %f",scale);
    };
    [self.cameraFuncView addSubview:self.scaleView];
    [self.scaleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.cameraFuncView).offset(-15);
        make.top.equalTo(self.cameraFuncView).offset(9);
        make.height.mas_equalTo(117);
        make.width.mas_equalTo(40);
    }];
    
    //闪光灯
    self.flashLampButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.flashLampButton addTarget:self action:@selector(flashLamp:) forControlEvents:UIControlEventTouchUpInside];
    [self.flashLampButton setImage:[UIImage imageNamed:@"light_off"] forState:UIControlStateNormal];
    [self.flashLampButton setImage:[UIImage imageNamed:@"light_on"] forState:UIControlStateSelected];
    [cameraFuncSubView addSubview:self.flashLampButton];
    [self.flashLampButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.right.equalTo(self.scaleView.mas_left).offset(-8);
        make.centerY.equalTo(closeButton);
    }];
    
    //前后相机切换
    UIButton *swichCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [swichCameraButton addTarget:self action:@selector(swichCamera:) forControlEvents:UIControlEventTouchUpInside];
    [swichCameraButton setImage:[UIImage imageNamed:@"swich_camera"] forState:UIControlStateNormal];
    [swichCameraButton setImage:[UIImage imageNamed:@"swich_camera"] forState:UIControlStateSelected];
    [cameraFuncSubView addSubview:swichCameraButton];
    [swichCameraButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.right.equalTo(self.flashLampButton.mas_left).offset(-8);
        make.centerY.equalTo(closeButton);
    }];
    
    
    self.contentPhoto = [[UIImageView alloc]init];
    self.contentPhoto.backgroundColor = [UIColor blackColor];
    self.contentPhoto.contentMode = UIViewContentModeScaleAspectFit;
    self.contentPhoto.hidden = YES;
    [self.view addSubview:self.contentPhoto];
    [self.contentPhoto mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.preview);
    }];
    
    self.contentVideo = [[UIView alloc]init];
    self.contentVideo.hidden = YES;
    [self.view addSubview:self.contentVideo];
    [self.contentVideo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.preview);
    }];
    
    UISwipeGestureRecognizer *recognizer_right = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe_right:)];
    [recognizer_right setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.preview addGestureRecognizer:recognizer_right];
    
    UISwipeGestureRecognizer *recognizer_left = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe_left:)];
    [recognizer_left setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.preview addGestureRecognizer:recognizer_left];
    
    
    
    self.bottomContentView = [[UIView alloc]init];
    [self.view addSubview:self.bottomContentView];
    [self.bottomContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.preview.mas_bottom);
        make.height.mas_equalTo(bottomHeight);
    }];
    
    
    self.photoCamera = [[CameraTypeView alloc]init];
    self.photoCamera.cameraTypeViewVM.contentStr = @"拍照";
    [self.bottomContentView addSubview:self.photoCamera];
    [self.photoCamera mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomContentView).offset(15);
        make.right.equalTo(self.bottomContentView.mas_centerX).offset(-20);
    }];
    
    self.videoCamera = [[CameraTypeView alloc]init];
    self.videoCamera.cameraTypeViewVM.contentStr = @"拍视频";
    [self.bottomContentView addSubview:self.videoCamera];
    [self.videoCamera mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.photoCamera);
        make.left.equalTo(self.bottomContentView.mas_centerX).offset(20);
    }];

    //拍照、录像
    CGSize size = CGSizeMake(85, 85);
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [self.bottomContentView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bottomContentView);
        make.size.mas_equalTo(size);
        make.top.equalTo(self.preview.mas_bottom).offset(75);
    }];
    
    self.circle = [[MLMCircleView alloc] initWithFrame:CGRectMake(0, 0, size.width,size.height) startAngle:-90 endAngle:270];
    self.circle.hidden = YES;
    self.circle.bottomWidth = 1;
    self.circle.progressWidth = 1.5;
    self.circle.fillColor = [UIColor redColor];
    self.circle.bgColor = [UIColor clearColor];
    self.circle.dotDiameter = 7;
    self.circle.dotImage = [UIImage imageNamed:@"evaluate_point"];
    [self.circle drawProgress];
    [view addSubview:self.circle];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.actionButton setImage:[UIImage imageNamed:@"takephoto"] forState:UIControlStateNormal];
    [self.actionButton addTarget:self action:@selector(action:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.actionButton];
    [self.actionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(75, 75));
        make.center.equalTo(view);
    }];
    
    UIView *placeholdLeftLine = [[UIView alloc]init];
    [self.bottomContentView addSubview:placeholdLeftLine];
    [placeholdLeftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomContentView);
        make.right.equalTo(self.actionButton.mas_left);
    }];
    //美颜
    UIButton *beautyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    beautyButton.hidden = YES;
    [beautyButton setImage:[UIImage imageNamed:@"beauty"] forState:UIControlStateNormal];
    [beautyButton addTarget:self action:@selector(beauty:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomContentView addSubview:beautyButton];
    [beautyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.actionButton);
        make.centerX.equalTo(placeholdLeftLine);
        make.size.mas_equalTo(CGSizeMake(30, 45));
    }];
    
    
    UIView *placeholdRightLine = [[UIView alloc]init];
    [self.bottomContentView addSubview:placeholdRightLine];
    [placeholdRightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bottomContentView);
        make.left.equalTo(self.actionButton.mas_right);
    }];
    //滤镜
    UIButton *filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.hidden = YES;
    [filterButton setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
    [filterButton addTarget:self action:@selector(filter:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomContentView addSubview:filterButton];
    [filterButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.actionButton);
        make.centerX.equalTo(placeholdRightLine);
        make.size.mas_equalTo(CGSizeMake(26, 45));
    }];
    
    self.operationContentView = [[OperationContentView alloc]init];
    @WeakObj(self);
    self.operationContentView.takePhoto = ^{
        [selfWeak takePhoto];
    };
    
    self.operationContentView.selectedImage = ^(UIImage * _Nonnull image) {
        selfWeak.contentPhoto.image = image;
        selfWeak.contentPhoto.hidden = NO;
    };
    
    self.operationContentView.cancelSelected = ^{
        selfWeak.contentPhoto.hidden = YES;
    };
    
    self.operationContentView.deletePhoto = ^(UIImage * _Nonnull delPhoto) {
        [[selfWeak.cameraViewControllerVM mutableArrayValueForKey:@"photosArray"] removeObject:delPhoto];
    };
    
    self.operationContentView.submit = ^{
        [SVProgressHUD showWithStatus:@"照片上传中..."];
        [selfWeak.cameraViewControllerVM uploadPhotoWithSuccess:^(NSMutableArray * _Nonnull urls) {
            [SVProgressHUD showSuccessWithStatus:@"照片上传成功"];
            if (selfWeak.getPhotos) {
                selfWeak.getPhotos(urls);
            }
            [selfWeak back];
        } fail:^(NSString * _Nonnull errorMessage) {
            [SVProgressHUD showErrorWithStatus:errorMessage];
        }];
    };
    
    
    self.operationContentView.function = ^(CameraFunctionModel * _Nonnull model) {
        selfWeak.cameraViewControllerVM.currentCameraFunctionModel = model;
    };
    
    
    self.operationContentView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.operationContentView];
    [self.operationContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(bottomHeight);
    }];

    self.filterFunctionView = [[FilterFunctionView alloc]init];
    self.filterFunctionView.dismiss = ^{
        if (selfWeak.cameraViewControllerVM.cameraOperationType == CameraType_TakePhoto) {
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
        }else{
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        }
    };
    
    self.filterFunctionView.confirm = ^{
        if (selfWeak.cameraViewControllerVM.cameraOperationType == CameraType_TakePhoto) {
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
        }else{
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        }
    };
    
    [self.view addSubview:self.filterFunctionView];
    [self.filterFunctionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(bottomHeight);
    }];
    
    self.beautyFunctionView = [[BeautyFunctionView alloc]init];
    self.beautyFunctionView.dismiss = ^{
        if (selfWeak.cameraViewControllerVM.cameraOperationType == CameraType_TakePhoto) {
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
        }else{
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        }
    };
    
    self.beautyFunctionView.confirm = ^{
        if (selfWeak.cameraViewControllerVM.cameraOperationType == CameraType_TakePhoto) {
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
        }else{
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        }
    };
    self.beautyFunctionView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.beautyFunctionView];
    [self.beautyFunctionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(bottomHeight);
    }];
    
    self.editFunctionView = [[EditFunctionView alloc]init];
    self.editFunctionView.backgroundColor = [UIColor blackColor];
    self.editFunctionView.dismiss = ^{
        if (selfWeak.cameraViewControllerVM.cameraOperationType == CameraType_TakePhoto) {
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
        }else{
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        }
    };
    self.editFunctionView.confirm = ^{
        if (selfWeak.cameraViewControllerVM.cameraOperationType == CameraType_TakePhoto) {
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
        }else{
            selfWeak.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        }
    };
    [self.view addSubview:self.editFunctionView];
    [self.editFunctionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(bottomHeight);
    }];
    
    self.videoEditFunctionView = [[VideoEditFunctionView alloc]init];
    self.videoEditFunctionView.function = ^(CameraFunctionModel * _Nonnull model) {
        selfWeak.cameraViewControllerVM.currentCameraFunctionModel = model;
    };
    self.videoEditFunctionView.submit = ^{
        [SVProgressHUD showWithStatus:@"视频上传中..."];
        [selfWeak.cameraViewControllerVM uploadVideoWithFileName:@"" success:^(NSString * _Nonnull videoURL) {
            [SVProgressHUD showSuccessWithStatus:@"视频上传成功"];
            if (selfWeak.getVideoURL) {
                selfWeak.getVideoURL(videoURL);
            }
            [selfWeak back];
        } fail:^(NSString * _Nonnull errorMessage) {
            [SVProgressHUD showErrorWithStatus:errorMessage];
        }];
    };
    self.videoEditFunctionView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.videoEditFunctionView];
    [self.videoEditFunctionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view.mas_bottom);
        make.height.mas_equalTo(bottomHeight);
    }];

    
    DebugLog(@"UI初始化完成");
    self.cameraViewControllerVM.cameraOperationType = CameraType_TakePhoto;
    
    [self initCamera];
    
    self.gTVReplayController = [[GTVReplayController alloc]init];
    [self.gTVReplayController setReplayDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self.gTVRecController setRenderView:self.preview withFrame:CGRectMake(0, 0, self.preview.frame.size.width, self.preview.frame.size.height)];
    [self.gTVReplayController setReplayRenderView:self.contentVideo withFrame:CGRectMake(0, 0, self.contentVideo.frame.size.width, self.contentVideo.frame.size.height)];
    [self.gTVRecController initRecord];
    [self.gTVRecController startPreivew];

    
    [self.preview bringSubviewToFront:self.cameraFuncView];
}

- (void)initCamera{
    
    NSString * tmpDir = [NSString stringWithFormat:@"%@", NSTemporaryDirectory()];
    
    self.gTVRecController = [[GTVRecController alloc] initWithRootPath:tmpDir
                                                 andDelegate:self];
}

- (void)closePage:(UIButton *)sender{
    DebugLog(@"关闭");
    [self back];
}

- (void)flashLamp:(UIButton *)sender{
    DebugLog(@"闪光灯");
    [self.gTVRecController setLightStatus:!self.gTVRecController.isLightOn];
    sender.selected = self.gTVRecController.isLightOn;
}

- (void)swichCamera:(UIButton *)sender{
    DebugLog(@"切换")
    [self.gTVRecController rotateCamera];

}

- (void)handleSwipe_right:(UISwipeGestureRecognizer *)sender{
    //拍照、拍视频过程中禁用滑动
    if (self.cameraViewControllerVM.recordStatus == RecordStatus_Recording || self.cameraViewControllerVM.photosArray.count>0) {
        return;
    }
    self.cameraViewControllerVM.cameraOperationType = CameraType_TakePhoto;
}

- (void)handleSwipe_left:(UISwipeGestureRecognizer *)sender{
    //拍照、拍视频过程中禁用滑动
    if (self.cameraViewControllerVM.recordStatus == RecordStatus_Recording || self.cameraViewControllerVM.photosArray.count>0) {
        return;
    }
    self.cameraViewControllerVM.cameraOperationType = CameraType_TakeVideo;
}

- (void)action:(UIButton *)sender{
    DebugLog(@"拍照");
    switch (self.cameraViewControllerVM.cameraOperationType) {
        case CameraType_TakePhoto:{
            [self takePhoto];
        }
            break;
            
        case CameraType_TakeVideo:{
            [self takeVideo];
        }
            break;
            
        default:
            break;
    }
}

- (void)takePhoto{
    @WeakObj(self);
    [self.gTVRecController takePicture:^(UIImage *image) {
        DebugLog(@"获取到图片image = %@",image);
        UIImage *newImage = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1.0)];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[selfWeak.cameraViewControllerVM mutableArrayValueForKey:@"photosArray"] addObject:newImage];
        });
    }];
}

- (void)takeVideo{
    
    if (self.cameraViewControllerVM.recordStatus == RecordStatus_RecordPrepare) {
        
        self.cameraViewControllerVM.recordStatus = RecordStatus_Recording;
        
    }else if (self.cameraViewControllerVM.recordStatus == RecordStatus_Recording){
        
        self.cameraViewControllerVM.recordStatus = RecordStatus_RecordEnd;
        
    }else if (self.cameraViewControllerVM.recordStatus == RecordStatus_RecordEnd){
        
        self.cameraViewControllerVM.recordStatus = RecordStatus_Recording;
    }
}

- (void)beauty:(UIButton *)sender{
    DebugLog(@"美颜");
    
}

- (void)filter:(UIButton *)sender{
    DebugLog(@"滤镜");

}

- (void)back{
    [self.gTVRecController destroyRecord];
}

#pragma mark-监听

#pragma mark-GTVReplayControllerDelegate
- (void) notifyReplayStatus:(NSString*)evt withInfo:(NSDictionary*)info{
    
}

#pragma mark-GTVRecControllerDelegate
- (void) notifyRecordStatus:(NSString*)evt withInfo:(NSDictionary*)info{
    DebugLog(@"notifyRecordStatus:%@ - %@", evt, info);
    if ([evt isEqualToString:kGTVRecEventInited]) {
        DebugLog(@"录制视频初始化完成");
        [self.gTVRecController clearAllCurrentClips];
    }else if( [evt isEqualToString:kGTVRecEventStarted] ) {
        DebugLog(@"开始录制");
        
    }else if( [evt isEqualToString:kGTVRecEventStopped] ) {
        
        NSArray *list = info[@"list"];
        NSDictionary *dic = list.lastObject;
        NSString *filname = dic[@"filename"];
        self.cameraViewControllerVM.localVideoPath = [NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(),filname];
        DebugLog(@"录制完成%@",self.cameraViewControllerVM.localVideoPath);
        self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_VideoEdit;
        
        //结束录制之后就开始播放录制
        self.contentVideo.hidden = NO;
        [self.gTVReplayController setReplayVideoPath:self.cameraViewControllerVM.localVideoPath];
        [self.gTVReplayController playVideo];
        
    }else if( [evt isEqualToString:kGTVRecEventDestroyed] ) {
        DebugLog(@"释放录制");
        [self.navigationController popViewControllerAnimated:YES];
    }else if( [evt isEqualToString:kGTVRecEventExported] ) {
        DebugLog(@"不知道怎么触发");
        
    }else if( [evt isEqualToString:kGTVRecEventRemoved] ) {
        DebugLog(@"录像删除");
        
    }else if( [evt isEqualToString:kGTVRecEventStatus] ) {
        DebugLog(@"录制过程中更新录制进度状态");
        NSArray *list = info[@"list"];
        NSDictionary *dic = list.lastObject;
        NSInteger duration = [self filterData:dic[@"duration"]].integerValue/1000;
        self.recordTimeLabel.text = [self getHHMMSSFromSS:duration];
        [self.circle setProgress:duration*1.0/self.maxRecordDuration];
        if (duration>=self.maxRecordDuration) {
            self.cameraViewControllerVM.recordStatus = RecordStatus_RecordEnd;
        }

    }else if( [evt isEqualToString:kGTVRecEventError] ) {
        DebugLog(@"录制异常");
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"photosArray"]) {
        
        self.operationContentView.operationContentViewVM.photos = self.cameraViewControllerVM.photosArray;
        
        if (self.cameraViewControllerVM.photosArray.count > 0) {
            if (self.cameraViewControllerVM.bottomShowToolType != BottomShowToolType_Photos) {
                self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Photos;
            }
        }else{
            self.contentPhoto.hidden = YES;
            if (self.cameraViewControllerVM.bottomShowToolType != BottomShowToolType_Original) {
                self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Original;
            }
        }
        
    }else if ([keyPath isEqualToString:@"cameraOperationType"]){
        
        CameraOperationType cameraOperationType = [self filterData:value].intValue;
        switch (cameraOperationType) {
            case CameraType_TakePhoto:{
                self.photoCamera.cameraTypeViewVM.isSelected = YES;
                self.videoCamera.cameraTypeViewVM.isSelected = NO;
                self.recordTimeLabel.hidden = YES;
            }
                break;
                
            case CameraType_TakeVideo:{
                self.photoCamera.cameraTypeViewVM.isSelected = NO;
                self.videoCamera.cameraTypeViewVM.isSelected = YES;
                self.cameraViewControllerVM.recordStatus = RecordStatus_RecordPrepare;
            }
                break;
                
            default:
                break;
        }
    }else if ([keyPath isEqualToString:@"bottomShowToolType"]){
        BottomShowToolType bottomShowToolType = [self filterData:value].intValue;
        switch (bottomShowToolType) {
            case BottomShowToolType_Original:{
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.operationContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.editFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.beautyFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.videoEditFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.filterFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.view layoutIfNeeded];
                    
                } completion:^(BOOL finished) {
                    
                }];
            }
                break;
                
            case BottomShowToolType_Photos:{
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.editFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.beautyFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.filterFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.videoEditFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.operationContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.bottom.equalTo(self.view);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
                
            }
                break;
                
            case BottomShowToolType_Edit:{
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.operationContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.beautyFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.filterFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.videoEditFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.editFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.bottom.equalTo(self.view);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
                
            }
                break;
                
            case BottomShowToolType_Beauty:{
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.operationContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.editFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    
                    [self.filterFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.videoEditFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.beautyFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.bottom.equalTo(self.view);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }
                break;
                
                
            case BottomShowToolType_Filter:{
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.operationContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.editFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.beautyFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.videoEditFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.filterFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.bottom.equalTo(self.view);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }
                break;
                
            case BottomShowToolType_VideoEdit:{
                
                [UIView animateWithDuration:0.3 animations:^{
                    
                    [self.operationContentView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.editFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.beautyFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.filterFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.right.equalTo(self.view);
                        make.top.equalTo(self.view.mas_bottom);
                        make.height.mas_equalTo(bottomHeight);
                    }];
                    
                    [self.videoEditFunctionView mas_remakeConstraints:^(MASConstraintMaker *make) {
                        
                        make.left.right.bottom.equalTo(self.view);
                        make.height.mas_equalTo(bottomHeight);
                        
                    }];
                    
                    [self.view layoutIfNeeded];
                } completion:^(BOOL finished) {
                    
                }];
            }
                break;
                
            default:
                break;
        }
        
    }else if ([keyPath isEqualToString:@"recordStatus"]){
        RecordStatus recordStatus = [self filterData:value].intValue;
        switch (recordStatus) {
            case RecordStatus_RecordPrepare:{
                self.recordTimeLabel.hidden = NO;
                self.recordTimeLabel.text = @"00:00";
            }
                break;
                
            case RecordStatus_Recording:{
                self.circle.hidden = NO;
                self.recordTimeLabel.hidden = NO;
                [self.gTVRecController startRecord];
            }
                break;
                
            case RecordStatus_RecordEnd:{
                self.recordTimeLabel.hidden = YES;
                self.circle.hidden = YES;
                self.recordTimeLabel.text = @"00:00";
                [self.gTVRecController stopRecord];
            }
                break;
                
            default:
                break;
        }
    }else if ([keyPath isEqualToString:@"currentCameraFunctionModel"]){
        CameraFunctionModel *currentCameraFunctionModel = value;
        if ([currentCameraFunctionModel isKindOfClass:[CameraFunctionModel class]]) {
            
            switch (currentCameraFunctionModel.functionMenuType) {
                case FunctionMenuType_Edit_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Edit;
                }
                    break;
                    
                case FunctionMenuType_Beauty_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Beauty;
                }
                    break;
                    
                case FunctionMenuType_Filter_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Filter;
                }
                    break;
                    
                case FunctionMenuType_Cut_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Filter;
                }
                    break;
                    
                case FunctionMenuType_Rotate_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Filter;
                }
                    break;
                    
                case FunctionMenuType_Brush_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Filter;
                }
                    break;
                    
                case FunctionMenuType_Mosaic_Photo:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Filter;
                }
                    break;
                    
                case FunctionMenuType_Beauty_Video:{
                    
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Beauty;

                }
                    break;
                    
                case FunctionMenuType_Filter_Video:{
                    self.cameraViewControllerVM.bottomShowToolType = BottomShowToolType_Filter;

                }
                    break;
                    
                case FunctionMenuType_AddText_Video:{
                }
                    break;
                    
                case FunctionMenuType_Cut_Video:{
                }
                    break;
                    
                case FunctionMenuType_AR_Video:{
                }
                    break;
                    
               
                    
                default:
                    break;
            }
        }
    }
}

- (void)dealloc{
    DebugLog(@"拍摄释放");
    [self.cameraViewControllerVM removeObserver:self forKeyPath:@"photosArray"];
    [self.cameraViewControllerVM removeObserver:self forKeyPath:@"cameraOperationType"];
    [self.cameraViewControllerVM removeObserver:self forKeyPath:@"bottomShowToolType"];
    [self.cameraViewControllerVM removeObserver:self forKeyPath:@"recordStatus"];
    [self.cameraViewControllerVM removeObserver:self forKeyPath:@"currentCameraFunctionModel"];
  
}

@end

