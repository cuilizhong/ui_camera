//
//  CameraViewControllerVM.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "CameraViewControllerVM.h"
#import "HjhApi.h"

@implementation CameraViewControllerVM

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.photosArray = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)uploadPhotoWithSuccess:(void(^)(NSMutableArray *urls))success fail:(void(^)(NSString *errorMessage))fail{
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    for (UIImage *image in self.photosArray) {
        NSData *data = UIImageJPEGRepresentation(image, 1.0);
        [tempArray addObject:data];
    }
    @WeakObj(self);
    [HjhApi uploadFileWithData:tempArray andFileName:@"" andFileType:@"image" callback:^(NSDictionary * _Nullable data, NSError * _Nullable error) {
        if (error) {
            
            fail(error.description);
            
        }else{
            
            id urlList = data[@"urlList"];
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            if ([selfWeak isArrayType:urlList]) {
                for (NSDictionary *dic in urlList) {
                    NSString *code = dic[@"code"];
                    NSString *url = dic[@"url"];
                    if ([code isEqualToString:@"ok"]) {
                        [tempArray addObject:url];
                    }
                }
                success(tempArray);
            }else{
                NSString *msg = data[@"msg"];
                fail(msg);
            }
        }
        
    }];
}

- (void)uploadVideoWithFileName:(NSString *)fileName success:(void(^)(NSString *videoURL))success fail:(void(^)(NSString *errorMessage))fail{
    NSData *data = [NSData dataWithContentsOfFile:self.localVideoPath];
    NSMutableArray *tempArray = [[NSMutableArray alloc]initWithObjects:data, nil];
    @WeakObj(self);
    [HjhApi uploadFileWithData:tempArray andFileName:fileName andFileType:@"video" callback:^(NSDictionary * _Nullable data, NSError * _Nullable error) {
        
        if (error) {
            
            fail(error.description);
            
        }else{
            
            id urlList = data[@"urlList"];
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            if ([selfWeak isArrayType:urlList]) {
                for (NSDictionary *dic in urlList) {
                    NSString *code = dic[@"code"];
                    NSString *url = dic[@"url"];
                    if ([code isEqualToString:@"ok"]) {
                        [tempArray addObject:url];
                    }
                }
                if (tempArray.count > 0) {
                    success(tempArray.firstObject);
                }
            }else{
                NSString *msg = data[@"msg"];
                fail(msg);
            }
        }
        
        DebugLog(@"data = %@",data);
    }];
}



+(CameraFunctionModel *)initCameraFunctionModelWithMenuImage:(UIImage *)menuImage functionMenuName:(NSString *)functionMenuName functionMenuType:(FunctionMenuType)functionMenuType{
    CameraFunctionModel *model = [[CameraFunctionModel alloc]init];
    model.functionMenuImage = menuImage;
    model.functionMenuName = functionMenuName;
    model.functionMenuType = functionMenuType;
    return model;
}

+(CameraFunctionModel *)getEdit_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"edit_photo"] functionMenuName:@"编辑" functionMenuType:FunctionMenuType_Edit_Photo];
}

+(CameraFunctionModel *)getBeauty_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"beauty"] functionMenuName:@"美颜" functionMenuType:FunctionMenuType_Beauty_Photo];
}

+(CameraFunctionModel *)getFilter_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"filter"] functionMenuName:@"滤镜" functionMenuType:FunctionMenuType_Filter_Photo];
}

+(CameraFunctionModel *)getCut_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"cut_photo_normal"] functionMenuName:@"裁剪" functionMenuType:FunctionMenuType_Cut_Photo];
}

+(CameraFunctionModel *)getRotate_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"rotate_photo_normal"] functionMenuName:@"旋转" functionMenuType:FunctionMenuType_Rotate_Photo];
}


+(CameraFunctionModel *)getBrush_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"brush_photo_normal"] functionMenuName:@"画笔" functionMenuType:FunctionMenuType_Brush_Photo];
}

+(CameraFunctionModel *)getMosaic_Photo{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"mosaic_photo_normal"] functionMenuName:@"马赛克" functionMenuType:FunctionMenuType_Mosaic_Photo];
}

+(CameraFunctionModel *)getBeauty_Video{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"beauty"] functionMenuName:@"美颜" functionMenuType:FunctionMenuType_Beauty_Video];
}

+(CameraFunctionModel *)getFilter_Video{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"filter"] functionMenuName:@"滤镜" functionMenuType:FunctionMenuType_Filter_Video];
}

+(CameraFunctionModel *)getAddText_Video{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"edit_photo"] functionMenuName:@"文字" functionMenuType:FunctionMenuType_AddText_Video];
}

+(CameraFunctionModel *)getCut_Video{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"cut_video"] functionMenuName:@"剪辑" functionMenuType:FunctionMenuType_Cut_Video];
}

+(CameraFunctionModel *)getAR_Video{
    
    return [CameraViewControllerVM initCameraFunctionModelWithMenuImage:[UIImage imageNamed:@"ar_video"] functionMenuName:@"AR" functionMenuType:FunctionMenuType_AR_Video];
}

@end
