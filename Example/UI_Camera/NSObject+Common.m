//
//  NSObject+Common.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "NSObject+Common.h"

@implementation NSObject (Common)


-(NSString *)getHHMMSSFromSS:(NSInteger)seconds{
    
    NSString *format_time = @"";
    //format of hour
    NSString *str_hour = [NSString stringWithFormat:@"%02ld",seconds/3600];
    //format of minute
    NSString *str_minute = [NSString stringWithFormat:@"%02ld",(seconds%3600)/60];
    //format of second
    NSString *str_second = [NSString stringWithFormat:@"%02ld",seconds%60];
    //format of time
    
    if (str_hour.integerValue > 0) {
        format_time = [NSString stringWithFormat:@"%@:%@:%@",str_hour,str_minute,str_second];
    }else{
        format_time = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
    }
    
    return format_time;
}

- (NSString *)getDateString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *currentDate = [NSDate date];
    NSString *currentDateString = [formatter stringFromDate:currentDate];
    return currentDateString;
}

- (NSString *)getCurrentDateStr{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSDate *currentDate = [NSDate date];
    NSString *currentDateString = [formatter stringFromDate:currentDate];
    return currentDateString;
}

-(NSString *)filterData:(id)data{
   if (data && ![data isKindOfClass:[NSNull class]]) {
       if ([data isKindOfClass:[NSString class]]) {
           if (![data isEqualToString:@"(null)"]) {
               return data;
           }
           
       }else if ([data isKindOfClass:[NSNumber class]]){
           
           return ((NSNumber *)data).stringValue;
       }
   }
   return @"";
}

- (BOOL)filterNilData:(id)data{
   if (data && ![data isKindOfClass:[NSNull class]]) {
       return YES;
   }
   return NO;
}

- (BOOL)isArrayType:(id)data{
   if ([self filterNilData:data]) {
       if ([data isKindOfClass:[NSArray class]] || [data isKindOfClass:[NSMutableArray class]]) {
           return YES;
       }
   }
   return NO;
}

- (BOOL)isDictionaryType:(id)data{
   if ([self filterNilData:data]) {
       if ([data isKindOfClass:[NSDictionary class]] || [data isKindOfClass:[NSMutableDictionary class]]) {
           return YES;
       }
   }
   return NO;
}


- (CGFloat)getStringHeightWithString:(NSString *)string font:(UIFont *)font maxWidth:(CGFloat)maxWidth numberOfLines:(NSInteger)numberOfLines{
   UILabel *label = [[UILabel alloc]init];
   label.numberOfLines = numberOfLines;
   label.font = font;
   label.text = string;
   return [label sizeThatFits:CGSizeMake(maxWidth,MAXFLOAT)].height;
}

- (CGFloat)getStringHeightWithString:(NSString *)string font:(UIFont *)font maxWidth:(CGFloat)maxWidth numberOfLines:(NSInteger)numberOfLines textAlignment:(NSTextAlignment)textAlignment{
   UILabel *label = [[UILabel alloc]init];
   label.textAlignment = textAlignment;
   label.numberOfLines = numberOfLines;
   label.font = font;
   label.text = string;
   return [label sizeThatFits:CGSizeMake(maxWidth,MAXFLOAT)].height;
}

- (CGFloat)getStringWidthWithString:(NSString *)string font:(UIFont *)font{
   UILabel *label = [[UILabel alloc]init];
   label.font = font;
   label.text = string;
   return [label sizeThatFits:CGSizeMake(MAXFLOAT,MAXFLOAT)].width;
}

@end
