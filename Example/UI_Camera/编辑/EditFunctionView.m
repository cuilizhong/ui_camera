//
//  EditFunctionView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "EditFunctionView.h"
#import "EditFunctionMenuView.h"

@interface EditFunctionView()


@end

@implementation EditFunctionView

- (instancetype)init{
    self = [super init];
    if (self) {
        
        
                
        UIView *topView = [[UIView alloc]init];
        [self addSubview:topView];
        [topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.mas_equalTo(50);
        }];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        cancelButton.titleLabel.font = Font(14);
        [topView addSubview:cancelButton];
        [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(30, 30));
            make.left.equalTo(topView).offset(10);
            make.centerY.equalTo(topView);
        }];
        
        UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [confirmButton addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
        [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        confirmButton.titleLabel.font = Font(14);
        [topView addSubview:confirmButton];
        [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(30, 30));
            make.right.equalTo(topView).offset(-10);
            make.centerY.equalTo(topView);
        }];
        
        UIView *placeholdView = [[UIView alloc]init];
        [self addSubview:placeholdView];
        [placeholdView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(topView.mas_bottom);
            make.bottom.equalTo(self);
        }];
        EditFunctionMenuView *editFunctionMenuView = [[EditFunctionMenuView alloc]init];
        [self addSubview:editFunctionMenuView];
        [editFunctionMenuView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.height.mas_equalTo(55);
            make.centerY.equalTo(placeholdView);
        }];
        
        
    }
    return self;
}

- (void)cancel:(UIButton *)sender{
    if (self.dismiss) {
        self.dismiss();
    }
}

- (void)confirm:(UIButton *)sender{
    if (self.confirm) {
        self.confirm();
    }
}


- (void)dealloc{
    
}

@end
