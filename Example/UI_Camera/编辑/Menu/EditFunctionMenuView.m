//
//  EditFunctionMenuView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "EditFunctionMenuView.h"
#import "VideoEditFunctionMenuItem.h"

@interface EditFunctionMenuView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;

@property(nonatomic,strong)NSMutableArray <CameraFunctionModel *>*photoEditFunctionArray;

@end

@implementation EditFunctionMenuView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.photoEditFunctionArray = [[NSMutableArray alloc]init];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getCut_Photo]];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getRotate_Photo]];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getBrush_Photo]];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getMosaic_Photo]];
        
        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        CGFloat itemWidth = 40;
        CGFloat itemHeight = 55;
        CGFloat edge = 20;
        layout.sectionInset = UIEdgeInsetsMake(0,edge, 0, edge);
        layout.itemSize = CGSizeMake(itemWidth,itemHeight);
        layout.minimumLineSpacing = (SCREEN_WIDTH - 2*edge - itemWidth*self.photoEditFunctionArray.count)*1.0/(self.photoEditFunctionArray.count-1);
        //2.初始化collectionView
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        [self.collectionView registerClass:[VideoEditFunctionMenuItem class] forCellWithReuseIdentifier:@"VideoEditFunctionMenuItem"];
    }
    return self;
}

#pragma mark-UICollectionViewDelegate,UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.photoEditFunctionArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoEditFunctionMenuItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoEditFunctionMenuItem" forIndexPath:indexPath];
    CameraFunctionModel *model = self.photoEditFunctionArray[indexPath.row];
    cell.contentImageView.image = model.functionMenuImage;
    cell.contentLabel.text = model.functionMenuName;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CameraFunctionModel *model = self.photoEditFunctionArray[indexPath.row];
    if (self.function) {
        self.function(model);
    }
}

- (void)dealloc{
    
}



@end
