//
//  HjhApiClient.h
//  HiARSDKComponentSample
//
//  Created by jinghao hou on 2020/12/14.
//  Copyright © 2020 MaJiangtao<majt@hiscene.com>. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking-umbrella.h>


NS_ASSUME_NONNULL_BEGIN

@interface HjhApiClient : AFHTTPSessionManager
+ (instancetype)sharedClient;
- (void)addHeader:(NSString*)key keyValue:(NSString*)keyValue;
- (void)removeHeader:(NSString*)key;
- (void)setHeader:(NSDictionary*)header;
- (NSDictionary*)getAllHeader;
@end

NS_ASSUME_NONNULL_END
