//
//  HjhApiClient.m
//  HiARSDKComponentSample
//
//  Created by jinghao hou on 2020/12/14.
//  Copyright © 2020 MaJiangtao<majt@hiscene.com>. All rights reserved.
//

#import "HjhApiClient.h"
static NSString * const APIBaseURLString = @"https://woxin2.jx139.com:8443/";
//static NSString * const APIBaseURLString = @"http://woxin.jx139.com/";


@interface HjhApiClient ()
@property (nonatomic,strong)NSMutableDictionary* header;
@end

@implementation HjhApiClient
+ (instancetype)sharedClient {
    static HjhApiClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[HjhApiClient alloc] initWithBaseURL:[NSURL URLWithString:APIBaseURLString]];
        _sharedClient.requestSerializer = [AFJSONRequestSerializer serializer];
//        _sharedClient.responseSerializer = [AFHTTPResponseSerializer serializer];
        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        //        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
        [securityPolicy setAllowInvalidCertificates:NO];
        [securityPolicy setValidatesDomainName:YES];
        _sharedClient.securityPolicy = securityPolicy;
        _sharedClient.header = [[NSMutableDictionary alloc] init];
        [_sharedClient.header setObject:@"internal" forKey:@"citycode"];
        [_sharedClient.header setObject:@"123456" forKey:@"cellphone"];
        [_sharedClient.header setObject:@"17.25.1" forKey:@"interfaceCode"];
    });
    return _sharedClient;
}
- (void)setHeader:(NSDictionary*)header{
    [self.header setDictionary:header];
}
- (NSDictionary*)getAllHeader{
    return self.header;
}
- (void)addHeader:(NSString*)key keyValue:(NSString*)keyValue{
    [self.header setObject:keyValue forKey:key];
}
- (void)removeHeader:(NSString*)key{
    [self.header removeObjectForKey:key];
}

@end
