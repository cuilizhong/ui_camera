//
//  HjhApi.h
//  HiARSDKComponentSample
//
//  Created by jinghao hou on 2020/12/14.
//  Copyright © 2020 MaJiangtao<majt@hiscene.com>. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^RequestCallBack) (NSDictionary * _Nullable data, NSError * _Nullable error);
NS_ASSUME_NONNULL_BEGIN

@interface HjhApi : NSObject
+ (void)getArInfo:(NSDictionary*)params callback:(RequestCallBack)callBack;

+ (void)uploadFileWithData:(NSMutableArray*)fileDataArray andFileName:(NSString *)fileName andFileType:(NSString *)fileType callback:(RequestCallBack)callBack;
@end

NS_ASSUME_NONNULL_END
