//
//  HjhApi.m
//  HiARSDKComponentSample
//
//  Created by jinghao hou on 2020/12/14.
//  Copyright © 2020 MaJiangtao<majt@hiscene.com>. All rights reserved.
//

#import "HjhApi.h"
#import "HjhApiClient.h"

@implementation HjhApi
+ (void)getArInfo:(NSDictionary*)params callback:(RequestCallBack)callBack{
    NSMutableDictionary* body = [NSMutableDictionary new];
    [body setObject:@{@"content":params} forKey:@"content"];
    [body setObject:@"17.25.1" forKey:@"msgType"];
    [body setObject:@"1|138" forKey:@"version"];
    [body setObject:[self getDateString] forKey:@"createTime"];
    
//    [HjhApiClient.sharedClient POST:@"interface/MsgPort" parameters:body headers:[HjhApiClient.sharedClient getAllHeader] progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        if(callBack){
//            callBack(responseObject,NULL);
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        if(callBack){
//            callBack(NULL,error);
//        }
//    }];
}

+ (void)uploadFileWithData:(NSMutableArray*)fileDataArray andFileName:(NSString *)fileName andFileType:(NSString *)fileType callback:(RequestCallBack)callBack{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"whoisyourdaddy" forHTTPHeaderField:@"auth1"];
        
    NSDictionary *header = @{
        @"auth1":@"whoisyourdaddy"
    };

    // http://woxin.jx139.com/multiUpload
    @WeakObj(self);
    [manager POST:@"http://woxin.jx139.com/multiUpload" parameters:@{} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for (int i = 0; i<fileDataArray.count; i++) {

            NSData *fileData = fileDataArray[i];

            if ([fileType isEqualToString:@"image"]) {

                NSString *tempFileName = [NSString stringWithFormat:@"%@_artime_%@_%d.png",[selfWeak getCurrentDateStr],@"15371083925",i];

                [formData appendPartWithFileData:fileData name:@"files" fileName:tempFileName mimeType:@"image/png"];

            }else if ([fileType isEqualToString:@"video"]){

                NSString *tempFileName = [NSString stringWithFormat:@"%@_artimevideo_%@.mp4",[selfWeak getCurrentDateStr],@"15371083925"];

                [formData appendPartWithFileData:fileData name:@"files" fileName:tempFileName mimeType:@"video/mp4"];
            }
        }

    } progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if(callBack){
            callBack(responseObject,NULL);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(callBack){
            callBack(NULL,error);
        }
    }];
    
    
    
    
    
    
    
    
    
//    [HjhApiClient.sharedClient POST:@"http://woxin.jx139.com/multiUpload" parameters:nil headers:header constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//
//        for (int i = 0; i<fileDataArray.count; i++) {
//
//            NSData *fileData = fileDataArray[i];
//
//            if ([fileType isEqualToString:@"image"]) {
//
//                NSString *tempFileName = [NSString stringWithFormat:@"%@_artime_%@_%d.png",[selfWeak getCurrentDateStr],@"15371083925",i];
//
//                [formData appendPartWithFileData:fileData name:@"files" fileName:tempFileName mimeType:@"image/png"];
//
//            }else if ([fileType isEqualToString:@"video"]){
//
//                NSString *tempFileName = [NSString stringWithFormat:@"%@_artimevideo_%@.mp4",[selfWeak getCurrentDateStr],@"15371083925"];
//
//                [formData appendPartWithFileData:fileData name:@"files" fileName:tempFileName mimeType:@"video/mp4"];
//            }
//        }
//
//
//    } progress:^(NSProgress * _Nonnull uploadProgress) {
//        NSLog(@"%f",1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        if(callBack){
//            callBack(responseObject,NULL);
//        }
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        if(callBack){
//            callBack(NULL,error);
//        }
//    }];
}



@end
