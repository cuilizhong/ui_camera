//
//  HjhLocationManager.h
//  HiARSDKComponentSample
//
//  Created by jinghao hou on 2020/12/14.
//  Copyright © 2020 MaJiangtao<majt@hiscene.com>. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^LocationSuccess) (double lat, double lng);
typedef void(^LocationFailed) (NSError *error);
@interface HjhLocationManager : NSObject
+ (HjhLocationManager *) sharedGpsManager;
+ (void) getLocationWithSuccess:(LocationSuccess)success Failure:(LocationFailed)failure;
+ (void) stop;
@end

NS_ASSUME_NONNULL_END
