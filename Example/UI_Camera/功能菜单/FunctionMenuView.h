//
//  FunctionMenuView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "BasicView.h"
#import "CameraViewControllerVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface FunctionMenuView : BasicView


@property(nonatomic,strong)void(^function)(CameraFunctionModel *model);

@end

NS_ASSUME_NONNULL_END
