//
//  FunctionMenuView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "FunctionMenuView.h"
#import "VideoEditFunctionMenuItem.h"
#import "CameraViewControllerVM.h"

@interface FunctionMenuView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray <CameraFunctionModel *>*photoEditFunctionArray;

@end

@implementation FunctionMenuView

- (instancetype)init{
    
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        self.photoEditFunctionArray = [[NSMutableArray alloc]init];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getEdit_Photo]];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getBeauty_Photo]];
        [self.photoEditFunctionArray addObject:[CameraViewControllerVM getFilter_Photo]];
                
        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        layout.minimumLineSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0,20, 0, 0);
        layout.itemSize = CGSizeMake(40,50);
        layout.minimumLineSpacing = 30;
        //2.初始化collectionView
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        self.collectionView.pagingEnabled = YES;
        [self addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        [self.collectionView registerClass:[VideoEditFunctionMenuItem class] forCellWithReuseIdentifier:@"VideoEditFunctionMenuItem"];
        
    }
    return self;
}

#pragma mark-UICollectionViewDelegate,UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.photoEditFunctionArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoEditFunctionMenuItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoEditFunctionMenuItem" forIndexPath:indexPath];
    CameraFunctionModel *model = self.photoEditFunctionArray[indexPath.row];
    cell.contentImageView.image = model.functionMenuImage;
    cell.contentLabel.text = model.functionMenuName;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CameraFunctionModel *model = self.photoEditFunctionArray[indexPath.row];
    if (self.function) {
        self.function(model);
    }
}


- (void)dealloc{
    
}


@end
