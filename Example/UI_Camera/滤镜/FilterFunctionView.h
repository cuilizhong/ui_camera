//
//  FilterFunctionView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/16.
//

#import "BasicView.h"
#import "FilterFunctionViewVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilterFunctionView : BasicView

@property(nonatomic,strong)FilterFunctionViewVM *filterFunctionViewVM;

@property(nonatomic,strong)void(^dismiss)(void);

@property(nonatomic,strong)void(^confirm)(void);

@end

NS_ASSUME_NONNULL_END
