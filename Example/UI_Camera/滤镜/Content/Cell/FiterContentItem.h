//
//  FiterContentItem.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicCollectionCell.h"
#import "FiterContentItemVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface FiterContentItem : BasicCollectionCell
@property(nonatomic,strong)FiterContentItemVM *fiterContentItemVM;
@end

NS_ASSUME_NONNULL_END
