//
//  FiterContentItem.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "FiterContentItem.h"

@interface FiterContentItem()

@property(nonatomic,strong)UIImageView *contentImageView;
@property(nonatomic,strong)UILabel *contentLabel;

@end

@implementation FiterContentItem

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.fiterContentItemVM = [[FiterContentItemVM alloc]init];
        
        [self.fiterContentItemVM addObserver:self forKeyPath:@"isSelected" options:NSKeyValueObservingOptionNew context:nil];
        
        [self.fiterContentItemVM addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];

        [self.fiterContentItemVM addObserver:self forKeyPath:@"titleStr" options:NSKeyValueObservingOptionNew context:nil];
        
        self.contentImageView = [[UIImageView alloc]init];
        self.contentImageView.image = [UIImage imageNamed:@"default"];
        self.contentImageView.layer.cornerRadius = 28;
        self.contentImageView.clipsToBounds = YES;
        [self.contentView addSubview:self.contentImageView];
        [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(56, 56));
            make.left.top.right.equalTo(self.contentView);
        }];
        
        self.contentLabel = [[UILabel alloc]init];
        self.contentLabel.text = @"自然";
        self.contentLabel.font = Font(12);
        self.contentLabel.textColor = [UIColor whiteColor];
        self.contentLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView);
            make.top.equalTo(self.contentImageView.mas_bottom).offset(10);
            make.bottom.equalTo(self.contentView);
        }];
        
        
    }
    return self;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"isSelected"]) {
        
    }else if ([keyPath isEqualToString:@"image"]){
        
    }else if ([keyPath isEqualToString:@"titleStr"]){
        
    }
}

- (void)dealloc{
    [self.fiterContentItemVM removeObserver:self forKeyPath:@"isSelected"];
    [self.fiterContentItemVM removeObserver:self forKeyPath:@"image"];
    [self.fiterContentItemVM removeObserver:self forKeyPath:@"titleStr"];
}

@end
