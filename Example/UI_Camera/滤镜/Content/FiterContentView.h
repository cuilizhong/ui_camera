//
//  FiterContentView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicView.h"
#import "FiterContentViewVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface FiterContentView : BasicView

@property(nonatomic,strong)FiterContentViewVM *fiterContentViewVM;

@end

NS_ASSUME_NONNULL_END
