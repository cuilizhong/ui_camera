//
//  FiterContentView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "FiterContentView.h"
#import "FiterContentItem.h"

@interface FiterContentView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;

@property(nonatomic,assign)NSInteger selectedIndex;

@end

@implementation FiterContentView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.fiterContentViewVM = [[FiterContentViewVM alloc]init];
        
        [self.fiterContentViewVM addObserver:self forKeyPath:@"isSelected" options:NSKeyValueObservingOptionNew context:nil];
        
        [self.fiterContentViewVM addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];

        [self.fiterContentViewVM addObserver:self forKeyPath:@"titleStr" options:NSKeyValueObservingOptionNew context:nil];

        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        layout.minimumLineSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(0,20, 0, 0);
        layout.minimumLineSpacing = 20;
        //2.初始化collectionView
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        [self.collectionView registerClass:[FiterContentItem class] forCellWithReuseIdentifier:@"FiterContentItem"];
        
    }
    return self;
}

#pragma mark-UICollectionViewDelegate,UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FiterContentItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FiterContentItem" forIndexPath:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(56, collectionView.frame.size.height);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedIndex = indexPath.row;
    [collectionView reloadData];
    
}


- (void)dealloc{
    
}

@end
