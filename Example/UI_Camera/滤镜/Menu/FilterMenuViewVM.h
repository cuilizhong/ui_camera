//
//  FilterMenuViewVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilterMenuViewVM : BasicViewModel

@property(nonatomic,strong)NSMutableArray *menuTitlesArray;

@end

NS_ASSUME_NONNULL_END
