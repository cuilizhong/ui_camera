//
//  FilterMenuItem.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "FilterMenuItem.h"

@interface FilterMenuItem()

@property(nonatomic,strong)UILabel *menuTitleLabel;

@property(nonatomic,strong)UIView *selectedBottomLine;

@end

@implementation FilterMenuItem

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.filterMenuItemVM = [[FilterMenuItemVM alloc]init];
        
        [self.filterMenuItemVM addObserver:self forKeyPath:@"contentStr" options:NSKeyValueObservingOptionNew context:nil];
        
        [self.filterMenuItemVM addObserver:self forKeyPath:@"isSelected" options:NSKeyValueObservingOptionNew context:nil];
        
        self.menuTitleLabel = [[UILabel alloc]init];
        self.menuTitleLabel.text = @"测试";
        self.menuTitleLabel.font = Font(13);
        self.menuTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.menuTitleLabel.textColor = ColorValue(0xCCCCCC);
        [self.contentView addSubview:self.menuTitleLabel];
        [self.menuTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.centerY.equalTo(self);
        }];
        
        self.selectedBottomLine = [[UIView alloc]init];
        self.selectedBottomLine.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.selectedBottomLine];
        [self.selectedBottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(5);
            make.right.equalTo(self).offset(-5);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(1);
        }];
        
        
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    id value = change[NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"contentStr"]) {
        
        NSString *contentStr = [self filterData:value];
        self.menuTitleLabel.text = contentStr;
        
    }else if ([keyPath isEqualToString:@"isSelected"]){
        
        BOOL isSelected = [self filterData:value].boolValue;
        self.selectedBottomLine.hidden = !isSelected;
        if (isSelected) {
            self.menuTitleLabel.textColor = [UIColor whiteColor];
        }else{
            self.menuTitleLabel.textColor = ColorValue(0xCCCCCC);
        }
    }
}

- (void)dealloc{
    
    [self.filterMenuItemVM removeObserver:self forKeyPath:@"contentStr"];
    [self.filterMenuItemVM removeObserver:self forKeyPath:@"isSelected"];
}

@end
