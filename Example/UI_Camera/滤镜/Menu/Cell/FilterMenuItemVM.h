//
//  FilterMenuItemVM.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilterMenuItemVM : BasicViewModel

@property(nonatomic,strong)NSString *contentStr;
@property(nonatomic,assign)BOOL isSelected;

@end

NS_ASSUME_NONNULL_END
