//
//  FilterMenuItem.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/17.
//

#import "BasicCollectionCell.h"
#import "FilterMenuItemVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilterMenuItem : BasicCollectionCell

@property(nonatomic,strong)FilterMenuItemVM *filterMenuItemVM;

@end

NS_ASSUME_NONNULL_END
