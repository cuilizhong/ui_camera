//
//  VideoEditFunctionView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "VideoEditFunctionView.h"
#import "VideoEditFunctionMenuView.h"

@interface VideoEditFunctionView()

@end

@implementation VideoEditFunctionView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.videoEditFunctionViewVM = [[VideoEditFunctionViewVM alloc]init];
        
        VideoEditFunctionMenuView *videoEditFunctionMenuView = [[VideoEditFunctionMenuView alloc]init];
        @WeakObj(self);
        videoEditFunctionMenuView.function = ^(CameraFunctionModel * _Nonnull model) {
            if (selfWeak.function) {
                selfWeak.function(model);
            }
        };
        [self addSubview:videoEditFunctionMenuView];
        [videoEditFunctionMenuView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).offset(40);
            make.left.right.equalTo(self);
            make.height.mas_equalTo(50);
        }];
        
        UIButton *nextStepButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextStepButton setBackgroundColor:[UIColor whiteColor]];
        [nextStepButton setTitleColor:ColorValue(0x333333) forState:UIControlStateNormal];
        [nextStepButton setTitle:@"下一步" forState:UIControlStateNormal];
        nextStepButton.layer.cornerRadius = 14;
        nextStepButton.titleLabel.font = Font(13);
        [nextStepButton addTarget:self action:@selector(nextStep:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:nextStepButton];
        [nextStepButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-20);
            make.size.mas_equalTo(CGSizeMake(70, 28));
            make.bottom.equalTo(self).offset(-30);
        }];
    }
    return self;
}

- (void)nextStep:(UIButton *)sender{
    if (self.submit) {
        self.submit();
    }
}

@end
