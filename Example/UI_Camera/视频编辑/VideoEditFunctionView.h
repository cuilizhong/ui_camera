//
//  VideoEditFunctionView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "BasicView.h"
#import "VideoEditFunctionViewVM.h"
#import "CameraViewControllerVM.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoEditFunctionView : BasicView

@property(nonatomic,strong)VideoEditFunctionViewVM *videoEditFunctionViewVM;

@property(nonatomic,strong)void(^submit)(void);

@property(nonatomic,strong)void(^function)(CameraFunctionModel *model);

@end

NS_ASSUME_NONNULL_END
