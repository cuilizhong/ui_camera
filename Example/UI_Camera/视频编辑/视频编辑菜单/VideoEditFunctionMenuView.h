//
//  VideoEditFunctionMenuView.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "BasicView.h"
#import "CameraViewControllerVM.h"


NS_ASSUME_NONNULL_BEGIN

@interface VideoEditFunctionMenuView : BasicView


@property(nonatomic,strong)void(^function)(CameraFunctionModel *model);

@end

NS_ASSUME_NONNULL_END
