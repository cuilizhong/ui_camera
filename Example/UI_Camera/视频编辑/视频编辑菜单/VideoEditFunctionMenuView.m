//
//  VideoEditFunctionMenuView.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "VideoEditFunctionMenuView.h"
#import "VideoEditFunctionMenuItem.h"


@interface VideoEditFunctionMenuView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;

@property(nonatomic,strong)NSMutableArray <CameraFunctionModel *>*videoEditFunctionArray;


@end

@implementation VideoEditFunctionMenuView

- (instancetype)init{
    
    self = [super init];
    if (self) {
        
        self.videoEditFunctionArray = [[NSMutableArray alloc]init];
        [self.videoEditFunctionArray addObject:[CameraViewControllerVM getBeauty_Video]];
        [self.videoEditFunctionArray addObject:[CameraViewControllerVM getFilter_Video]];
        [self.videoEditFunctionArray addObject:[CameraViewControllerVM getAddText_Video]];
        [self.videoEditFunctionArray addObject:[CameraViewControllerVM getCut_Video]];
        [self.videoEditFunctionArray addObject:[CameraViewControllerVM getAR_Video]];
        
        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        CGFloat itemWidht = 40;
        CGFloat itemHeight = 50;
        CGFloat edge = 20;
        NSInteger maxCount = self.videoEditFunctionArray.count;
        layout.sectionInset = UIEdgeInsetsMake(0,edge, 0, edge);
        CGFloat spacing = ((SCREEN_WIDTH -  edge*2)*1.0-maxCount*itemWidht)/(maxCount-1);
        layout.itemSize = CGSizeMake(itemWidht,itemHeight);
        layout.minimumLineSpacing = spacing;
        layout.minimumInteritemSpacing = 0;
        
        
        //2.初始化collectionView
        self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
            
        [self.collectionView registerClass:[VideoEditFunctionMenuItem class] forCellWithReuseIdentifier:@"VideoEditFunctionMenuItem"];
        
    }
    return self;
}

#pragma mark-UICollectionViewDelegate,UICollectionViewDataSource
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.videoEditFunctionArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
     
    VideoEditFunctionMenuItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoEditFunctionMenuItem" forIndexPath:indexPath];
    CameraFunctionModel *model = self.videoEditFunctionArray[indexPath.row];
    cell.contentImageView.image = model.functionMenuImage;
    cell.contentLabel.text = model.functionMenuName;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CameraFunctionModel *model = self.videoEditFunctionArray[indexPath.row];
    if (self.function) {
        self.function(model);
    }
}


- (void)dealloc{
}

@end
