//
//  VideoEditFunctionMenuItem.m
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "VideoEditFunctionMenuItem.h"

@interface VideoEditFunctionMenuItem()



@end

@implementation VideoEditFunctionMenuItem

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentImageView = [[UIImageView alloc]init];
        [self.contentView addSubview:self.contentImageView];
        [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.centerX.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
        
        self.contentLabel = [[UILabel alloc]init];
        self.contentLabel.font = Font(13);
        self.contentLabel.textColor = [UIColor whiteColor];
        self.contentLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.contentLabel];
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.contentView);
        }];
        
    }
    return self;
}

@end
