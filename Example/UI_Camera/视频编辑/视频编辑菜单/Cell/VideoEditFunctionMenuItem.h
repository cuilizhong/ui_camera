//
//  VideoEditFunctionMenuItem.h
//  Camera
//
//  Created by Cui lizhong on 2020/12/19.
//

#import "BasicCollectionCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoEditFunctionMenuItem : BasicCollectionCell

@property(nonatomic,strong)UIImageView *contentImageView;
@property(nonatomic,strong)UILabel *contentLabel;

@end

NS_ASSUME_NONNULL_END
