
#import <UIKit/UIKit.h>

//! Project version number for GTVideoLib.
FOUNDATION_EXPORT double GTVideoLibVersionNumber;

//! Project version string for GTVideoLib.
FOUNDATION_EXPORT const unsigned char GTVideoLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GTVideoLib/PublicHeader.h>

#import <GTVideoLib/GTVDef.h>
#import <GTVideoLib/GTVideoTool.h>
#import <GTVideoLib/GTVRecController.h>
#import <GTVideoLib/GTVReplayController.h>
