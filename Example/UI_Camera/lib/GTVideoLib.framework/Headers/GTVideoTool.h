
#import <Foundation/Foundation.h>

@interface GTVideoTool : NSObject

// 抽取JPEG数据
//  moive:原始视频文件路径 toFolder:导出图片存储的文件夹 withinRange:类型为NSRange，毫秒级的抽取范围 andNumLimit:最多抽取帧数
//  返回的NSArray中的元素是一个字典类型，包含两个key，filepath和timestamp 分别对应图片路径和时间戳
+ (NSArray*) syncGrabJPEG:(NSString*)moive toFolder:(NSString*)f withinRange:(NSRange)timerange andNumLimit:(int)max;

// 可以指定缩放因子(scale必须大于0.0，建议小于=1.0)
+ (NSArray*) syncGrabAndScaleJPEG:(NSString*)movie toFolder:(NSString*)f withinRange:(NSRange)timerange andNumLimit:(int)max inScaleFactor:(float)scale;

// 抽取原始YUV帧，一般用于WEBP合成
+ (NSArray*) syncGrabRawYUV:(NSString*)moive toFolder:(NSString*)f withinRange:(NSRange)timerange andNumLimit:(int)max;

// 可以指定缩放因子(scale必须大于0.0，建议小于=1.0)
+ (NSArray*) syncGrabAndScaleRawYUV:(NSString*)moive toFolder:(NSString*)f withinRange:(NSRange)timerange andNumLimit:(int)max inScaleFactor:(float)scale;

// 音视频合并
+ (void) mergeAudio:(NSString*)a andVideo:(NSString*)v toFile:(NSString*)m;

// 导入转换视频（调整成全I帧模式） destPath需要指定全路径 /xxx/xxx/xxx.mp4
typedef void (^GTVImportProgressFunc)(int percent);
+ (BOOL) importMovie:(NSString*)srcPath toPath:(NSString*)destPath withProgress:(GTVImportProgressFunc)cb;

@end
