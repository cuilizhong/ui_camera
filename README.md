# UI_Camera

[![CI Status](http://img.shields.io/travis/Cui lizhong/UI_Camera.svg?style=flat)](https://travis-ci.org/Cui lizhong/UI_Camera)
[![Version](https://img.shields.io/cocoapods/v/UI_Camera.svg?style=flat)](http://cocoapods.org/pods/UI_Camera)
[![License](https://img.shields.io/cocoapods/l/UI_Camera.svg?style=flat)](http://cocoapods.org/pods/UI_Camera)
[![Platform](https://img.shields.io/cocoapods/p/UI_Camera.svg?style=flat)](http://cocoapods.org/pods/UI_Camera)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UI_Camera is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UI_Camera"
```

## Author

Cui lizhong, cui_li_zhong@163.com

## License

UI_Camera is available under the MIT license. See the LICENSE file for more info.
