#!/bin/bash

pods=()

writePodspec(){
  podspec_pod_dependency_falg='<<<====s.dependency====>>>'
  podspec_content=`cat *.podspec`
  # echo "$podspec_content"
  # echo '======================'
  pods_str=$(IFS=$'\n'; echo -e "${pods[*]}")
  pods_content=`echo -e "$podspec_pod_dependency_falg\n$pods_str"`
  # echo "$pods_content"
  podspec_content=${podspec_content/\#${podspec_pod_dependency_falg}/\#${pods_content}}
  # podspec_content=${podspec_content/\# ====s.dependency=== \#/fuck}

  # echo "$podspec_content"
  echo "$podspec_content" > *.podspec
}

updatePod() {
  echo -e "\n\033[34mget your pod version ...\033[0m"
  pod --version
  checkExec

  echo -e "\n\033[34mbegin update local pod repo ...\033[0m"
  pod repo update CLZSpecs
  checkExec

  # pod update --project-directory=Example --verbose
  # 这里的Example为相对路径，后面改为绝对路径
  echo -e "\n\033[34mbegin update ...\033[0m"
  pod update --verbose --no-repo-update --project-directory=Example
  checkExec

}

checkExec() {
  if [ $? -eq 0 ]; then
    # success
    echo "success"
  else
    # fail
    echo "fail"
    exit 0
  fi
}



echo -e "\n\nplease enter pod (ex: 'AFNetworking'   '~> 0.1.0'), complete with twice return key"
echo "==========================================="



while :
do
  # if read -t 5 -e -p "pod : " pod
  if read -t 100 -e pod
  then

    if [ -z "$pod" ]; then
      # 判断是否有空输入
      echo -e "\n#------------------------------------------"
      # for x in ${pods[@]}; do
      #   echo "*******pod: ${x}"
      # done

      for ((i=0;i<${#pods[*]};i++)); do
        echo "#${pods[i]}"
      done
      echo -e "#------------------------------------------"

      writePodspec
      updatePod
      exit 0
    fi

    pods+=("      s.dependency $pod")

  else
    echo -e "\n==========================================="
    echo "sorry,too slow"
    exit 0
  fi
done
