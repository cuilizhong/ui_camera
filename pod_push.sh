#!/bin/bash

podVersion=''
podNote=''

checkExec() {
  if [ $? -eq 0 ]; then
    # success
    echo "success"
  else
    # fail
    echo "fail"
    exit 0
  fi
}

# 获取当前版本信息
getPodCurrentInfo() {
  podspec_file=`ls *.podspec`
  podspec_file_name=$(basename $podspec_file .podspec)
  pod search $podspec_file_name
}

# 获取private pod 版本号
getPodVersion() {
  echo -e "\n#------------------------------------------"
  read -e -p "# pod version : " version
  if [ -z "$version" ]; then
    echo "pod version not be empty"
    getPodVersion
  fi

  # pod version 写入podspec file
  podspec_content=`cat *.podspec`
  echo -e "$podspec_content" | sed -E "s/s.version          = '[0-9]+(\.[0-9]+)*'/s.version          = '$version'/g"  > *.podspec

  # echo $version
  podVersion=$version
}

# 获取版本 说明
getPodNote() {
  echo -e "\n#------------------------------------------"
  read -e -p "# pod note of current version : " note
  if [ -z "$note" ]; then
    echo "pod note can not be empty"
    getPodNote
  fi
  # echo $note
  podNote=$note
}

gitCommit() {

  # read -e -p "commit message : " commitMessage
  # if [ -z "$commitMessage" ]; then
  #   echo "commit message can not be empty"
  #   gitCommit
  # fi
  #
  # echo $commitMessage

  getPodCurrentInfo
  getPodVersion
  getPodNote


  git add .
  checkExec

  git add -A
  checkExec

  git commit -m "$podNote"
  checkExec

  echo -e "\n\033[34mgit push origin ...\033[0m"
  git push origin HEAD
  checkExec
}

gitPushTag() {

  git tag -m "$podNote" "$podVersion"
  checkExec

  echo -e "\n\033[34mgit push tags ...\033[0m"
  git push --tags
  checkExec
}

podLint() {
  echo -e "\n\033[34mpod lint ...\033[0m"
  # pod lib lint --allow-warnings --sources=CLZSpecs,master --use-libraries
  # 通过module @import 导入 dependency， 则不需要 --use-libraries
  pod lib lint --allow-warnings --sources=CLZSpecs,master --verbose
  checkExec
}

podPush() {
  echo -e "\n\033[34mpod repo push ...\033[0m"
  podspec_file=`ls *.podspec`
  # pod repo push CLZSpecs $podspec_file --allow-warnings --sources=CLZSpecs,master --use-libraries
  # 通过module @import 导入 dependency， 则不需要 --use-libraries
  pod repo push CLZSpecs $podspec_file --allow-warnings --sources=CLZSpecs,master --verbose
  checkExec

  echo -e "\n\033[34mreset pod search index ...\033[0m"
  rm ~/Library/Caches/CocoaPods/search_index.json
  checkExec

  podspec_file_name=$(basename $podspec_file .podspec)
  #因创建search 索引耗时，所以在后台进行,不用输出
  pod search $podspec_file_name &> /dev/null &
}

podUpdate() {
  echo -e "\n\033[34mget your pod version ...\033[0m"
  pod --version
  checkExec

  echo -e "\nbegin update local pod repo ...\033[0m"
  pod repo update CLZSpecs
  checkExec

  # pod update --project-directory=Example --verbose
  # 这里的Example为相对路径，后面改为绝对路径
  echo -e "\nbegin update ...\033[0m"
  pod update --verbose --no-repo-update --project-directory=Example
  checkExec
}

podUpdate
podLint
gitCommit
gitPushTag
podPush
