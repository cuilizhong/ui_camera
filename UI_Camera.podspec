#
# Be sure to run `pod lib lint UI_Camera.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UI_Camera'
  s.version          = '1.0'
  s.summary          = 'Camera'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  # s.homepage         = 'http://gitlab-cm.retech.local/ios-product-line/ui-UI_Camera'
  # s.homepage         = 'http://gitlab-cm.retech.local/ios-product-line/ui-ui_camera'
  s.homepage         = 'https://gitee.com/cuilizhong/ui_camera'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Cui lizhong' => 'cui_li_zhong@163.com' }
  # s.source           = { :git => 'http://gitlab-cm.retech.local/ios-product-line/ui-UI_Camera.git', :branch => '1001', :tag => s.version.to_s }
  # 因老板gitlab git地址都为小写 故对POD_NAME进行小写转换POD_NAME_DOWNCASE 更新新版gitlab 还原为POD_NAME
  s.source           = { :git => 'https://gitee.com/cuilizhong/ui_camera.git', :branch => '1001', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '7.0'

  s.source_files = 'UI_Camera/Classes/**/*'

  s.resource_bundles = {
    'UI_Camera' => ['UI_Camera/Assets/**/*.{xcassets,json,imageset,png,jpg,strings,db,sqlite,sqlite3}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

  
  #<<<====s.dependency====>>>

end
